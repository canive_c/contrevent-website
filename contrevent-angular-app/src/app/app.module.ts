import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy, PathLocationStrategy} from '@angular/common';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProgrammationComponent } from './programmation/programmation.component';
import { BilleterieComponent } from './billeterie/billeterie.component';
import { CashlessComponent } from './cashless/cashless.component';
import { FestivalComponent } from './festival/festival.component';
import { PartenairesComponent } from './partenaires/partenaires.component';
import { InfosComponent } from './infos/infos.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { FooterComponent } from './common/footer/footer.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ProgrammationComponent,
    BilleterieComponent,
    CashlessComponent,
    FestivalComponent,
    PartenairesComponent,
    InfosComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    Location, {provide: LocationStrategy, useClass: PathLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
