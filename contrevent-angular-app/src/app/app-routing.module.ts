import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgrammationComponent } from './programmation/programmation.component';
import { BilleterieComponent } from './billeterie/billeterie.component';
import { CashlessComponent } from './cashless/cashless.component';
import { FestivalComponent } from './festival/festival.component';
import { PartenairesComponent } from './partenaires/partenaires.component';
import { InfosComponent } from './infos/infos.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {path: '',                    component: HomeComponent },
  { path: 'programmation',      component: ProgrammationComponent },
  { path: 'billeterie',         component: BilleterieComponent },
  { path: 'cashless',           component: CashlessComponent },
  { path: 'le-festival',        component: FestivalComponent },
  { path: 'partenaires',        component: PartenairesComponent },
  { path: 'infos-pratiques',    component: InfosComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
