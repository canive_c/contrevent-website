import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public isNavbarCollapsed = false;

  constructor(public router: Router) { }

  ngOnInit() {
  }

  clickNavbar() {
    if (this.isNavbarCollapsed === false) {
     this.isNavbarCollapsed = true;
    } else if (this.isNavbarCollapsed === true) {
     this.isNavbarCollapsed = false;
    }
  }

  moveMobileNavbar() {
    if (this.isNavbarCollapsed === true) {
      const styles = {
        transition: '0.8s',
        opacity: 1,
        marginTop: '-60px'
      };
      return styles;
    }

    if (this.isNavbarCollapsed === false) {
      const styles = {
        transition: '0.8s',
        opacity: 0,
        marginTop: '-1200px'
      };
      return styles;
    }
  }

  moveMobileSpan1() {
    if (this.isNavbarCollapsed === true) {
      const styles = {
        transform: 'rotate(45deg) translate(-2px, -1px)',
        transition: '0.5s',
        marginTop: '10px'
      };
      return styles;
    }

    if (this.isNavbarCollapsed === false) {
      const styles = {
        'transform-origin': '4px 0px',
        transition: '0.5s'
      };
      return styles;
    }
  }

  moveMobileSpan2() {
    if (this.isNavbarCollapsed === true) {
      const styles = {
        opacity: 0,
        transform: 'rotate(0deg) scale(0.2, 0.2)',
        transition: '0.5s'
      };
      return styles;
    }

    if (this.isNavbarCollapsed === false) {
      const styles = {
        'transform-origin': '4px 0px',
        transition: '0.5s'
      };
      return styles;
    }
  }

  moveMobileSpan3() {
    if (this.isNavbarCollapsed === true) {
      const styles = {
        transform: 'rotate(-45deg) translate(0, -1px)',
        transition: '0.5s',
        marginTop: '-22px'
      };
      return styles;
    }

    if (this.isNavbarCollapsed === false) {
      const styles = {
        'transform-origin': '4px 0px',
        transition: '0.5s'
      };
      return styles;
    }
  }

}
